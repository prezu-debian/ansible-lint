FROM debian:bullseye

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y ansible ansible-lint git
