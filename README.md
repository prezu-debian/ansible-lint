# ansible-lint

## Pipeline Status

[![pipeline status](https://gitlab.com/prezu-debian/ansible-lint/badges/main/pipeline.svg)](https://gitlab.com/prezu-debian/ansible-lint/-/commits/main)

## Description

The sole purpose of this project is to build an `ansible-lint` Docker image,
that other Ansible projects in this group can use for linting.
